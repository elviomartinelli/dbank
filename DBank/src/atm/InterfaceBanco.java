/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author 31281354
 */
public interface InterfaceBanco extends Remote {

    public void deposito(int conta, int valor)
            throws RemoteException;

    public void saque(int conta, int valor)
            throws RemoteException;

    public void transferencia(int conta_orig, int conta_dest, int valor)
            throws RemoteException;

    public int saldo(int conta)
            throws RemoteException;
}
