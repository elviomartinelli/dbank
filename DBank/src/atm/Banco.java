/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atm;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;



/**
 *
 * @author 31281354
 */
public class Banco extends UnicastRemoteObject implements InterfaceBanco, Serializable {

    private HashMap<Integer,Integer> contaUsuario; // conta do usuario // saldo do usuario

    public Banco() throws RemoteException {
        
    }

    @Override
    public void deposito(int conta, int valor) throws RemoteException {
//implementacoes
        int saldo=contaUsuario.get(conta);
        saldo +=valor;
        contaUsuario.put(conta, saldo);
    }

    @Override
    public void saque(int conta, int valor) throws RemoteException {
//implementacoes
        int saldo = contaUsuario.get(conta);
        saldo -= valor;
        contaUsuario.put(conta, saldo);
    }

    @Override
    public void transferencia(int conta_orig, int conta_dest, int valor)
            throws RemoteException {
// implementacoes
                int saldoOrg = contaUsuario.get(conta_orig);
                int saldoDest = contaUsuario.get(conta_dest);
                int saldo = saldoOrg -valor;
                contaUsuario.put(conta_orig, saldo);
                saldo = saldoDest + valor;
                contaUsuario.put(conta_dest,saldo);
    }

    @Override
    public int saldo(int conta) throws RemoteException {
        System.out.println("Saldo na conta: "+conta+" de R$ "+contaUsuario.get(conta));
        return 0;
//implementacoes
    }

    public static void main(String args[]) throws Exception {
//init servidor Banco
        InterfaceBancoImpl robj =new InterfaceBancoImpl();
        InterfaceBanco stub =(InterfaceBanco) UnicastRemoteObject.exportObject(robj,0);
        
        LocateRegistry.createRegistry(1099);
        Registry registry = LocateRegistry.getRegistry();
        registry.rebind("Hello!", stub);
        System.out.println("Server is up and whait the girl like you!");
    }

    String add(int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
